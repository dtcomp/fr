#!/usr/bin/python3
import sys
import argparse as ARG
import math as M
from engineering_notation import EngNumber as EN


E={}
E[3]=(1.0, 2.2, 4.7)
E[6]=(1.0, 1.5, 2.2, 3.3, 4.7, 6.8)
E[12]=(1.0, 1.2, 1.5, 1.8, 2.2, 2.7, 3.3, 3.9, 4.7, 5.6, 6.8, 8.2)
E[24]=(1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2, 2.4, 2.7, 3.0, 3.3, 3.6, 3.9, 4.3, 4.7, 5.1, 5.6, 6.2, 6.8, 7.5, 8.2, 9.1)
E[48]=(1.00, 1.05, 1.10, 1.15, 1.21, 1.27, 1.33, 1.40, 1.47, 1.54, 1.62, 1.69, 1.78, 1.87, 1.96, 2.05, 2.15, 2.26, 2.37, 2.49, 2.61, 2.74, 2.87, 3.01, 3.16, 3.32, 3.48, 3.65, 3.83, 4.02, 4.22, 4.42, 4.64, 4.87, 5.11, 5.36, 5.62, 5.90, 6.19, 6.49, 6.81, 7.15, 7.50, 7.87, 8.25, 8.66, 9.09, 9.53)
E[96]=(1.00, 1.02, 1.05, 1.07, 1.10, 1.13, 1.15, 1.18, 1.21, 1.24, 1.27, 1.30, 1.33, 1.37, 1.40, 1.43, 1.47, 1.50, 1.54, 1.58, 1.62, 1.65, 1.69, 1.74, 1.78, 1.82, 1.87, 1.91, 1.96, 2.00, 2.05, 2.10, 2.15, 2.21, 2.26, 2.32, 2.37, 2.43, 2.49, 2.55, 2.61, 2.67, 2.74, 2.80, 2.87, 2.94, 3.01, 3.09, 3.16, 3.24, 3.32, 3.40, 3.48, 3.57, 3.65, 3.74, 3.83, 3.92, 4.02, 4.12, 4.22, 4.32, 4.42, 4.53, 4.64, 4.75, 4.87, 4.99, 5.11, 5.23, 5.36, 5.49, 5.62, 5.76, 5.90, 6.04, 6.19, 6.34, 6.49, 6.65, 6.81, 6.98, 7.15, 7.32, 7.50, 7.68, 7.87, 8.06, 8.25, 8.45, 8.66, 8.87, 9.09, 9.31, 9.53, 9.76)
E[192]=(1.00, 1.01, 1.02, 1.04, 1.05, 1.06, 1.07, 1.09, 1.10, 1.11, 1.13, 1.14, 1.15, 1.17, 1.18, 1.20, 1.21, 1.23, 1.24, 1.26, 1.27, 1.29, 1.30, 1.32, 1.33, 1.35, 1.37, 1.38, 1.40, 1.42, 1.43, 1.45, 1.47, 1.49, 1.50, 1.52, 1.54, 1.56, 1.58, 1.60, 1.62, 1.64, 1.65, 1.67, 1.69, 1.72, 1.74, 1.76, 1.78, 1.80, 1.82, 1.84, 1.87, 1.89, 1.91, 1.93, 1.96, 1.98, 2.00, 2.03, 2.05, 2.08, 2.10, 2.13, 2.15, 2.18, 2.21, 2.23, 2.26, 2.29, 2.32, 2.34, 2.37, 2.40, 2.43, 2.46, 2.49, 2.52, 2.55, 2.58, 2.61, 2.64, 2.67, 2.71, 2.74, 2.77, 2.80, 2.84, 2.87, 2.91, 2.94, 2.98, 3.01, 3.05, 3.09, 3.12, 3.16, 3.20, 3.24, 3.28, 3.32, 3.36, 3.40, 3.44, 3.48, 3.52, 3.57, 3.61, 3.65, 3.70, 3.74, 3.79, 3.83, 3.88, 3.92, 3.97, 4.02, 4.07, 4.12, 4.17, 4.22, 4.27, 4.32, 4.37, 4.42, 4.48, 4.53, 4.59, 4.64, 4.70, 4.75, 4.81, 4.87, 4.93, 4.99, 5.05, 5.11, 5.17, 5.23, 5.30, 5.36, 5.42, 5.49, 5.56, 5.62, 5.69, 5.76, 5.83, 5.90, 5.97, 6.04, 6.12, 6.19, 6.26, 6.34, 6.42, 6.49, 6.57, 6.65, 6.73, 6.81, 6.90, 6.98, 7.06, 7.15, 7.23, 7.32, 7.41, 7.50, 7.59, 7.68, 7.77, 7.87, 7.96, 8.06, 8.16, 8.25, 8.35, 8.45, 8.56, 8.66, 8.76, 8.87, 8.98, 9.09, 9.20, 9.31, 9.42, 9.53, 9.65, 9.76, 9.88)


argp = ARG.ArgumentParser(description="Find RCL combos of 2 components or matching standard values")
group = argp.add_mutually_exclusive_group()
argp.add_argument('-e','--series', help='Define your standard value series to search (E24)', type=int, choices=[3,6,12,24,48,96,192],default=24)
argp.add_argument('-t','--tolerance', help='Define acceptable tolerance for search (1.0 percent)', type=float, default=1.0)
argp.add_argument('-d','--decades', help='Number of decades to try (3)', type=int, default=3, choices=range(1,7))
group.add_argument('-l','--limit', help='Limit number of output values per component (nolimit)', type=int ,default=-1)
group.add_argument('-1','--best', dest='best', action='store_const', const=True, help='Output closest match only (False)', default=False)
argp.add_argument('values', help='Component Values', type=EN, nargs='+')
args=argp.parse_args()


def normal(val):
    m=0
    bmax=BASE[len(BASE)-1]
    bmin=BASE[0]
    v=val
    if val > M.ceil(bmax):
        while v > M.ceil(bmax):
            v=v/10.0
            m+=1
    else:
        if  val < M.floor(bmin):
            while v < M.floor(bmin):
                v=v*10.0
                m-=1
    return m, v


def denormal(m,val):
    return val*M.pow(10.0,m)


def series(*args):
    return float(sum(args))


def parallel(*args):
    return 1.0 / sum([ 1.0 / float(r) for r in args ])


def logz(t,m,z,z1,z2,q,v,d):
    z=denormal(m,z); z1=denormal(m,z1);  d=denormal(m,d)
    txt1="{:>8}".format("{}".format(EN(z,2,6)))
    txt2="{}".format(EN(z1,2,4))
    if t == '*':
        txt3="{:<8}".format(" ")
    else:
        z2=denormal(m,z2);
        txt3="{}".format(EN(z2,2,4))
    txt4="{}".format(EN(d,2,5))
    BUF[d]=(\
    t,\
    "{:>8}".format(txt1),\
    "{:>8}".format(txt2),\
    "{:<8}".format(txt3),\
    "{:>8}".format(txt4),\
    "{:>-8.3%}".format((max(z,v)/min(z,v)-1.0)))


def findz2(Z,tol,decades=3,best=False):
    val=float(Z)
    m,v=normal(val)
    t=pow(10.0,-m)*(val*(float(tol)/100.0))
    xtol=t;
    found=False
    res=list(map( lambda x: M.isclose(x,v,abs_tol=xtol), BASE ))
    if True in res:
        for index in list(filter(lambda x: res[x], range(len(res)))):
            logz('*',m,BASE[index],BASE[index],0.0,Z,val,BASE[index]-v)
        found=True
    else:
        for r1 in BASE:
            for r2 in BASE:
                for d in range(decades):
                    dec=round(pow(10.0,d))
                    r2sx=r2/dec; r2px=r2*dec
                    if r1<v and r2sx<v:
                        rx=series(r1,r2sx)
                        diff=M.fabs(rx-v)
                        if diff <= xtol:
                            if best:
                                xtol=diff
                            found=True
                            logz('+',m,rx,r1,r2sx,Z,val,diff)
                    elif r1>v and r2px>v:
                        rx=parallel(r1,r2px)
                        diff=M.fabs(rx-v)
                        if diff <= xtol:
                            if best:
                                xtol=diff
                            found=True
                            logz('|',m,rx,r1,r2px,Z,val,diff)

    return found, denormal(m,t)



def printlog(best=False,limit=-1):
    i=0
    for k in sorted(BUF, key=lambda D: M.fabs(D)):
        print(\
        "Combo:{:>8}".format(BUF[k][1]),\
        BUF[k][2],\
        BUF[k][0],\
        BUF[k][3],\
        "delta:",\
        BUF[k][4],\
        BUF[k][5],\
        sep='')
        if best:
            return
        i+=1
        if limit>0 and i>=limit:
            return


def head():
    print("-----------------------------------------------------",sep='')

BUF={}
BASE=E[args.series]

head()
print( 'Series:E', "{:<3}".format(args.series),\
        "{:>14}".format("Decades:"),args.decades,\
        "{:>19}".format("Tolerance:"),"{:>8.3%}".format(args.tolerance/100.0),sep='')

for q in args.values:
    succeed,atol=findz2(q,args.tolerance,args.decades,args.best)
    head()

    if not succeed:
        print('Whoops!\nNo combos for E',args.series,' ', q,\
        '/{:.3%}'.format(args.tolerance/100.0)," in ",args.decades," decade(s)" ,sep='')
        print('Mebbe try relaxing one or more parameters:\n\tTolerance, Series or Decades...')

    print("Value:", "{:>8}".format("{}".format(EN(float(q),2,6))), \
    "                abstol:", "{:>8}".format("{}".format(EN(atol,2,5))),sep='')
    printlog(args.best,args.limit)
    BUF={}                              # Cleanup...
head()








